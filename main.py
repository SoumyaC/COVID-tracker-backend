from pymongo import MongoClient
import flask
import json
app = flask.Flask(__name__)
app.config["DEBUG"] = True

def main():
    client= MongoClient('mongodb://root:rootpassword@localhost:9100/admin')
    @app.route('/', methods=['GET'])
    def home():
        msg = {"message": "connected"}
        return json.dumps(msg)

    @app.route('/provinces')
    def provinces():
        db = client.covid_db
        provinces = db.covid19_prov.find()
        results = []

        for province in provinces:
            del province['_id']
            results.append(province)
        client.close()

        return json.dumps(results)

    @app.route('/covid19data')
    def covid19data():
        db = client.covid_db
        entries = db.covid19_full.find()
        results = []

        for entry in entries:
            del entry['_id']
            results.append(entry)
        client.close()

        return json.dumps(results)
    app.run()

    # client.close()
if __name__ == '__main__':
    main()